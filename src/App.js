import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import IndexPage from './pages/IndexPage';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <div>
          <Route exact path="/" component={IndexPage} />
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
