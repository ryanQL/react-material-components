import React, { Component } from 'react';
import EnhancedTable from '../components/EnhancedTable';

const sampleData = [
  { id: 78431, title: "Hotel boasts fine views over Stroud’s Golden Valley", date: "21/09/2018" },
  { id: 78384, title: "New Zealand Commercial Property Investor Confidence Survey (Q3, 2018)", date: "21/09/2018" },
  { id: 78383, title: "New Zealand Residential Property Market Outlook Survey (Q3, 2018)", date: "21/09/2018" },
  { id: 78350, title: "UK Property Snapshot (September, 2018)", date: "21/09/2018" },
  { id: 78264, title: "Hinkley Point C legacy already visible two years on from historic agreement", date: "20/09/2018" },
  { id: 78174, title: "Top Locations in Asia - Technology Sector (September, 2018)", date: "19/09/2018" },
  { id: 78173, title: "Yangon Condominium (Q2, 2018)", date: "19/09/2018" },
  { id: 78172, title: "Evolving Dynamics Makkah Retail Overview (Q1, 2018)", date: "19/09/2018" },
  { id: 78140, title: "Host latest phase of Lendlease’s new mixed-use development, The Timberyard Deptford", date: "19/09/2018" },
  { id: 77949, title: "Metro Vancouver Investment Report (H1, 2018)", date: "17/09/2018"}
];

const header = [
  { id: 'id', numeric: false, disablePadding: true, label: 'Id' },
  { id: 'title', numeric: false, disablePadding: false, label: 'Title' },
  { id: 'date', numeric: false, disablePadding: false, label: 'Date' },
];

class IndexPage extends Component {
  render() {
    return (
      <EnhancedTable 
        data={sampleData} 
        title="Posts"
        allowSelection={false}
        header={header}
        hideIdColumn={true}
        order="desc"
        orderBy="title"
        rowsPerPage={10}
        linkTextColumn="title"
        linkToBaseUrl="/post"
      />
    )
  }
}

export default IndexPage;